AWSTemplateFormatVersion: 2010-09-09
Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: 10.1.0.0/16
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
        - Key: Name
          Value: !Join ['', [!Ref "AWS::StackName", '-VPC']]
  InternetGateway:
    Type: AWS::EC2::InternetGateway
    DependsOn: VPC
    Properties:
      Tags:
        - Key: Name
          Value: !Join ['', [!Ref "AWS::StackName", '-IGW']]
  AttachGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway
  VpcRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
  InternetRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref VpcRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway
  Subnet1RouteAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties: 
      RouteTableId: !Ref VpcRouteTable
      SubnetId: !Ref Subnet1
  Subnet2RouteAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties: 
      RouteTableId: !Ref VpcRouteTable
      SubnetId: !Ref Subnet2
  Subnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      CidrBlock: 10.1.1.0/24
      AvailabilityZone: !Select [ 0, !GetAZs ]
      Tags:
        - Key: Name
          Value: !Join ['', [!Ref "AWS::StackName", '-Subnet1']]
  Subnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      CidrBlock: 10.1.2.0/24
      AvailabilityZone: !Select [ 1, !GetAZs ]
      Tags:
        - Key: Name
          Value: !Join ['', [!Ref "AWS::StackName", '-Subnet2']]
  EC2TargetGroupV2:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckIntervalSeconds: 30
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 15
      HealthyThresholdCount: 5
      Matcher:
        HttpCode: '200'
      Name: EC2TargetGroupV2
      Port: 80
      Protocol: HTTP
      TargetGroupAttributes:
      - Key: deregistration_delay.timeout_seconds
        Value: '20'
      Targets:
      - Id: !Ref WordpressServer
        Port: 80
      UnhealthyThresholdCount: 3
      VpcId: !Ref VPC

  ELBSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: ELB Security Group
      VpcId: !Ref VPC
      SecurityGroupIngress:
      - IpProtocol: tcp
        FromPort: 80
        ToPort: 80
        CidrIp: 0.0.0.0/0
      - IpProtocol: tcp
        FromPort: 443
        ToPort: 443
        CidrIp: 0.0.0.0/0
  ALBListenerV2:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
        - Type: forward
          TargetGroupArn: !Ref EC2TargetGroupV2
      LoadBalancerArn: !Ref WordpressLoadBalancerV3
      Port: 80
      Protocol: HTTP

  WordpressLoadBalancerV3:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Scheme: internal
      Subnets:
        - !Ref Subnet1
        - !Ref Subnet2
      SecurityGroups:
        - !GetAtt ELBSecurityGroup.GroupId
  WordpressSecurityGroupV2:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupName: WordpressSecurityGroupV2
      GroupDescription: WordpressSecurityGroupV2
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: '443'
          ToPort: '443'
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: 0.0.0.0/0
      VpcId: !Ref VPC
  WordpressServerKeyPair:
    Type: AWS::EC2::KeyPair
    Properties:
      KeyName: WordpressServerKeyPair
  WordpressServer:
    Type: 'AWS::EC2::Instance'
    Properties:
      ImageId: ami-0c9e6b72793f015e8
      InstanceType: t2.micro
      KeyName: WordpressServerKeyPair
      NetworkInterfaces:
        - DeviceIndex: 0
          GroupSet:
            - Ref: WordpressSecurityGroupV2
          SubnetId: !Ref Subnet1
          AssociatePublicIpAddress: false
      PrivateDnsNameOptions:
        HostnameType: resource-name
        EnableResourceNameDnsARecord: true
      Tags:
        - Key: Name
          Value: WordpressServer
  HelperVmV2:
    Type: 'AWS::EC2::Instance'
    Properties:
      ImageId: ami-0c956e207f9d113d5
      InstanceType: t2.micro
      KeyName: WordpressServerKeyPair
      NetworkInterfaces:
        - DeviceIndex: 0
          GroupSet:
            - Ref: WordpressSecurityGroupV2
          SubnetId: !Ref Subnet1
          AssociatePublicIpAddress: true
      Tags:
        - Key: Name
          Value: HelperVm
  PhotosBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: imiano-photos-bucket
      NotificationConfiguration:
        LambdaConfigurations:
          - Event: s3:ObjectCreated:*
            Function: !GetAtt ResizeLambda.Arn
  PhotosResizedBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: imiano-photos-bucket-resized
  LambdaExecutorRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: lambda-chest-executor
      AssumeRolePolicyDocument: |
        {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": "lambda.amazonaws.com"
              },
              "Action": "sts:AssumeRole"
            }
          ]
        }
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
  ImageResizeRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: image-resize-executor
      AssumeRolePolicyDocument: |
        {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": "lambda.amazonaws.com"
              },
              "Action": "sts:AssumeRole"
            }
          ]
        }
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
  ChestLambda:
    Type: AWS::Lambda::Function
    Properties:
      Code:
        S3Bucket: imiano-books-lambda-code
        S3Key: chest/v1.zip
      Role: !GetAtt 'LambdaExecutorRole.Arn'
      Runtime: go1.x
      Handler: chest
      Timeout: 30
      MemorySize: 512
      Environment:
        Variables:
          IMIANO_CHEST_PATH_PREFIX: "/chest"
  ResizeLambda:
    Type: AWS::Lambda::Function
    Properties:
      Code:
        S3Bucket: imiano-books-lambda-code
        S3Key: resize/my-deployment-package.zip
      Role: !GetAtt 'ImageResizeRole.Arn'
      Runtime: python3.8
      Handler: lambda_function.lambda_handler
      Timeout: 30
      MemorySize: 1024
  ImageResizeS3Policy:
    Type: 'AWS::IAM::Policy'
    Properties:
      PolicyName: allowAccessForResize
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Action:
              - 's3:GetObject'
            Resource: !Sub '${PhotosBucket.Arn}/*'
          - Effect: Allow
            Action:
              - 's3:PutObject'
            Resource: !Sub '${PhotosResizedBucket.Arn}/*'
      Roles:
        - !Ref ImageResizeRole
  ApiAccessLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      RetentionInDays: 7
  LambdaLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      RetentionInDays: 7
      LogGroupName: !Sub "/aws/lambda/${ChestLambda}"
  ResizeLambdaLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      RetentionInDays: 7
      LogGroupName: !Sub "/aws/lambda/${ResizeLambda}"
  HttpApi:
    Type: AWS::ApiGatewayV2::Api
    Properties:
      Name: Lambda gw
      ProtocolType: HTTP
  GWVpcLink:
    Type: AWS::ApiGatewayV2::VpcLink
    Properties:
      Name: GWVpcLink
      SecurityGroupIds:
        - !Ref WordpressSecurityGroupV2
      SubnetIds:
        - !Ref Subnet1
        - !Ref Subnet2
  LambdaIntegration:
    Type: AWS::ApiGatewayV2::Integration
    Properties:
      ApiId: !Ref HttpApi
      IntegrationType: AWS_PROXY
      IntegrationMethod: POST
      IntegrationUri: !Sub 'arn:aws:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/${ChestLambda.Arn}/invocations'
      PayloadFormatVersion: 1.0
  LambdaRouteChest:
    Type: 'AWS::ApiGatewayV2::Route'
    DependsOn:
      - LambdaIntegration
    Properties:
      ApiId: !Ref HttpApi
      RouteKey: "ANY /chest/{subPath+}"
      Target: !Join
        - /
        - - integrations
          - !Ref LambdaIntegration
  WordpressIntegration:
    Type: AWS::ApiGatewayV2::Integration
    Properties:
      ApiId: !Ref HttpApi
      ConnectionId: !Ref GWVpcLink
      ConnectionType: VPC_LINK
      IntegrationType: HTTP_PROXY
      IntegrationMethod: ANY
      IntegrationUri: !Ref ALBListenerV2
      PayloadFormatVersion: 1.0
  WordpressRoute:
    Type: 'AWS::ApiGatewayV2::Route'
    DependsOn:
      - WordpressServer
    Properties:
      ApiId: !Ref HttpApi
      RouteKey: "$default"
      Target: !Join
        - /
        - - integrations
          - !Ref WordpressIntegration
  ApiDeployment:
    Type: 'AWS::ApiGatewayV2::Deployment'
    DependsOn: LambdaRouteChest
    Properties:
      ApiId: !Ref HttpApi
  ApiStage:
    Type: 'AWS::ApiGatewayV2::Stage'
    Properties:
      StageName: '$default'
      ApiId: !Ref HttpApi
      AutoDeploy: true
      AccessLogSettings:
        Format: >
          $context.identity.sourceIp $context.identity.caller
          $context.identity.user [$context.requestTime]
          "$context.httpMethod $context.path $context.protocol"
          $context.status $context.responseLength $context.requestId $context.extendedRequestId
        DestinationArn: !GetAtt ApiAccessLogGroup.Arn
  ApiGatewayInvokePermissionForChest:
    Type: AWS::Lambda::Permission
    Properties:
      Action: lambda:InvokeFunction
      FunctionName: !GetAtt ChestLambda.Arn
      Principal: apigateway.amazonaws.com
  S3InvokePermissionForResize:
    Type: AWS::Lambda::Permission
    Properties:
      Action: lambda:InvokeFunction
      FunctionName: !GetAtt ResizeLambda.Arn
      Principal: s3.amazonaws.com
      SourceArn: !Sub 'arn:aws:s3:::${PhotosBucket}'
  AllowS3AccessPolicy:
    Type: 'AWS::IAM::Policy'
    Properties:
      PolicyName: allowAccessFromLambda
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Action:
              - 's3:GetObject'
            Resource: !Sub '${PhotosResizedBucket.Arn}/*'
          - Effect: Allow
            Action:
              - 's3:ListBucket'
            Resource: !Sub '${PhotosResizedBucket.Arn}'
      Roles:
        - !Ref LambdaExecutorRole
  usersDynamoDBTable:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
        - AttributeName: User
          AttributeType: S
      KeySchema:
        - AttributeName: User
          KeyType: HASH
      ProvisionedThroughput:
        ReadCapacityUnits: 2
        WriteCapacityUnits: 2
      TableName: usersTable
  albumsDynamoDBTable:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
        - AttributeName: BucketSubdirName
          AttributeType: S
      KeySchema:
        - AttributeName: BucketSubdirName
          KeyType: HASH
      ProvisionedThroughput:
        ReadCapacityUnits: 2
        WriteCapacityUnits: 2
      TableName: albumsTable
  AllowDbAccessPolicy:
    Type: 'AWS::IAM::Policy'
    Properties:
      PolicyName: allowDbAccessFromLambda
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Action:
              - 'dynamodb:GetItem'
            Resource: !GetAtt 'usersDynamoDBTable.Arn'
          - Effect: Allow
            Action:
              - 'dynamodb:GetItem'
              - 'dynamodb:PutItem'
              - 'dynamodb:Scan'
            Resource: !GetAtt 'albumsDynamoDBTable.Arn'
      Roles:
        - !Ref LambdaExecutorRole
