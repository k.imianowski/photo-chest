set -e
set -x

sudo docker build -t go_aws_builder docker-builder/

sudo docker run -it -v $(pwd)/:/chest go_aws_builder

rsync -av --delete chest/templates packages/

(
  cd packages
  zip -r chest.zip chest templates
)
