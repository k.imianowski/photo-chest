import boto3
import os
import sys
import uuid
from urllib.parse import unquote_plus
from PIL import Image
import PIL.Image

s3_client = boto3.client('s3')
THUMBNAIL_SIZE = (128, 128)
FULL_SIZE = (1024, 1024)

def resize_image(image_path, thumbnail_path, full_size_path):
    with Image.open(image_path) as image:
        image.thumbnail(FULL_SIZE)
        image.save(full_size_path)
        image.thumbnail(THUMBNAIL_SIZE)
        image.save(thumbnail_path)

def lambda_handler(event, context):
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        key = unquote_plus(record['s3']['object']['key'])
        print(f"Will try to resize image from {bucket} key {key}")
        tmpkey = key.replace('/', '')
        download_path = '/tmp/{}{}'.format(uuid.uuid4(), tmpkey)
        full_size_path = '/tmp/resized-{}'.format(tmpkey)
        thumbnail_path = '/tmp/thumbnail-{}'.format(tmpkey)
        s3_client.download_file(bucket, key, download_path)
        print(f"Downloaded source file")
        resize_image(download_path, thumbnail_path, full_size_path)
        print(f"File resized")
        s3_client.upload_file(thumbnail_path, '{}-resized'.format(bucket), f"thumbnail/{key}")
        print(f"Thumbnail uploaded")
        s3_client.upload_file(full_size_path, '{}-resized'.format(bucket), f"resized/{key}")
        print(f"Full size uploaded")

def main():
    pass

if __name__ == '__main__':
    main()
