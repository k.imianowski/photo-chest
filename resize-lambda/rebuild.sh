
set -x
set -e

if [[ ! -d pacskage/ ]];
then
  . python3.8-env/bin/activate
  pip install --target ./package Pillow
fi

( cd package && zip -r ../my-deployment-package.zip . )

zip -g my-deployment-package.zip lambda_function.py

