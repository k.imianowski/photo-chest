set -e
set -x

aws s3api put-object --bucket imiano-books-lambda-code --key resize/my-deployment-package.zip --body ./my-deployment-package.zip

fName=$(aws lambda list-functions | jq -r '."Functions"[].FunctionName' | grep ResizeLambda)

aws lambda update-function-code --function-name "$fName" --s3-bucket imiano-books-lambda-code --s3-key resize/my-deployment-package.zip
