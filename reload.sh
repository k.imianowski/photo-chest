set -e
set -x

aws s3api put-object --bucket imiano-books-lambda-code --key chest/v1.zip --body ./packages/chest.zip

fName=$(aws lambda list-functions | jq -r '."Functions"[].FunctionName' | grep ChestLambda)

aws lambda update-function-code --function-name "$fName" --s3-bucket imiano-books-lambda-code --s3-key chest/v1.zip
