
# Goal

My goal was to explore different services provided by AWS and study what can be done using them. For this I needed to create a sample web application. The idea came from real life. I needed an application to be able to share private photo albums guarded by a password – I called it a photo chest.

The whole source code can be found at https://gitlab.com/k.imianowski/photo-chest

A working demo can be found at https://imiano.click/chest/album/show/a1, password is mo3fmFM7pVeBbJqf

Please note that it is more of a technical demo than a production-ready solution. For example changing admin user password can be only done by manual change in DynamoDB table, and photos can be inserted only via direct S3 access.

# Solution

Since the application was supposed to be used for rather infrequent use, I decided to go with AWS Lambda. It is supposed to be excellent in scaling up and down, so it should be more cost effective than having an EC2 instance up and running all the time and doing mostly nothing.

Overall infrastructure of the whole solution is managed by CloudFormation, described in a YAML file, held in git repository. The great advantage of such approach is the ease of keeping track of changes and making sure the entire architecture is correct and nothing was changed by mistake. Any cons? Web console wizards are great for people who have little experience with aws. Creating api gateway, for example, is just a few clicks in the web console, while doing so in CloudFormation requires learning about and manually describing all its components – integration, route, stage and deployment.

The application code is written in Go using Gin as a web framework. The decision to use Go was also an experiment, as I had no prior experience with this language. Go is, however, widely used in web application and advertised as a right language for such solutions.

Go application communicates with DynamoDB for user data and image metadata. Photos are stored in an S3 bucket and downloaded by Go application and served back to the end user as needed.

# Resizing photos

After some initial testing of the approach, it turned out that with the default memory size (128 MB) and default timeout (3 seconds), a lambda does not manage to download and serve back a few-MB image from S3. Increasing memory (note that AWS scales CPU up with memory “accordingly”) and timeout, the lambda had enough time, but some better solution was needed still. Also the chosen UI library required the image to be inserted twice – once as a thumbnail and once as a full photo. I decided to use two S3 buckets – one for uploading original photos, and another for storing scaled-down photos to show in the application and even more scaled-down to use as thumbnails. A Python Lambda takes care of resizing the original photos – it was taken from some tutorial and tweaked a little to suit my needs.
Photo access authentication

When an album page is requested, and a proper password provided, the application generates a JWT token used for accessing the actual photos. The token contains album ID and expiration time. It is then added to the photo links in the generated album page like this: `<img src="/chest/thumbnail/a1/photo1.jpg?accessToken=XXXX>` When a browser sends a request to retrieve the photo, the token is verified if it belongs to the correct album and is not expired.

# AWS security

Both the lambda functions have each own IAM Policy, which has only permissions required for it’s functionality. EC2 serving wordpress is in a private VPC subnet, and has no public IP. Security groups allow incoming traffic only to HTTP(S) and SSH ports.

# WordPress

The go application and wordpress are served on the same domain. To achieve this, I used API gateway which was already present in the setup. However, wordpress is installed on a regular EC2 VM. Unfortunately, API Gateway does not allow directly routing requests to an EC2 instance. I needed to add a somewhat dumb load balancer, with only one target.

# General observations

Serverless means you don’t need to worry about actual machines building your infrastructure. But it’s a double-edged sword. You also have no machines to connect to and view logs or verify if something works there the way you expected. When you make some error in configuration of the different services, you sometimes just get HTTP 503 error and you are on your own. My example was setting load balancer scheme to internet-facing instead of internal. I spent quite some time wondering why the page is visible when accessed by a load balancer directly, but not via API Gateway. Turns out internet-facing load balancer are not only accessible from the internet, but also not accessible from the internal network.
