package model

import "io"

type Image struct {
	N      int64
	Reader io.Reader
}

type UserCredentials struct {
	User     string
	Pepper   string
	PassHash string
}

type Album struct {
	BucketSubdirName string
	Name             string
	Password         string
	Description      string
}
