#!/bin/bash

set -e
set -x

~/go/bin/mockgen -source awsIntegration/awsIntegration.go >mock_awsIntegration/awsIntegration.go
~/go/bin/mockgen -source controller/passwordManager.go >mock_controller/passwordManagerMock.go
~/go/bin/mockgen -source controller/authRequired.go >mock_controller/authRequired.go
~/go/bin/mockgen -source controller/imageAccessToken.go >mock_controller/imageAccessToken.go
