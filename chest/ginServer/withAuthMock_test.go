package ginServer_test

import (
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"imiano.pl/chest/controller"
	"imiano.pl/chest/ginServer"
	"imiano.pl/chest/mock_awsIntegration"
	"imiano.pl/chest/mock_controller"
)

type testWithAuthMock struct {
	ginServerTest
	authRequiredMock *mock_controller.MockAuthRequired
}

func newTestWithAuthMock(t *testing.T) *testWithAuthMock {
	mockController := gomock.NewController(t)
	authRequiredMock := mock_controller.NewMockAuthRequired(mockController)
	authRequiredMock.EXPECT().Handler(gomock.Any()).AnyTimes().Do(func(c *gin.Context) { c.Next() })

	return &testWithAuthMock{
		ginServerTest: ginServerTest{
			Assert:              assert.New(t),
			mockController:      mockController,
			mockAws:             mock_awsIntegration.NewMockAwsIntegration(mockController),
			mockPasswordManager: mock_controller.NewMockPasswordManager(mockController),
		},
		authRequiredMock: authRequiredMock,
	}
}

func (thisTest *testWithAuthMock) CreateTestRouter() *gin.Engine {
	controllerDeps := controller.ControllerDependencies{
		Aws:          thisTest.mockAws,
		PassManager:  thisTest.mockPasswordManager,
		TokenManager: thisTest.mockTokenManager,
	}
	deps := &ginServer.RouterDependencies{
		Ctrl:          controller.NewController(controllerDeps),
		TemplatesPath: "../templates/*",
		AuthRequired:  thisTest.authRequiredMock,
	}
	return ginServer.CreateRouter(deps)
}
