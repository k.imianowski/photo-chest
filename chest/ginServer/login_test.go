package ginServer_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"imiano.pl/chest/mock_awsIntegration"
	"imiano.pl/chest/mock_controller"
	"imiano.pl/chest/model"
)

var sessionCookieRegexStr = "^session=[0-9A-Za-z_-]+;"

type loginTest struct {
	ginServerTest
}

func newLoginTest(t *testing.T) *loginTest {
	mockController := gomock.NewController(t)

	return &loginTest{
		ginServerTest: ginServerTest{
			Assert:              assert.New(t),
			mockController:      mockController,
			mockAws:             mock_awsIntegration.NewMockAwsIntegration(mockController),
			mockPasswordManager: mock_controller.NewMockPasswordManager(mockController),
		},
	}
}

func (thisTest *loginTest) GivenPasswordCorrect(username, password string) {
	thisTest.mockPasswordManager.EXPECT().CheckPassword(username, password).Return(true).Times(1)
}

func (thisTest *loginTest) GivenPasswordInCorrect(username, password string) {
	thisTest.mockPasswordManager.EXPECT().CheckPassword(username, password).Return(false).Times(1)
}

func (thisTest *loginTest) GivenEmptyAlbumList() {
	thisTest.mockAws.EXPECT().ListAlbums().Return([]model.Album{}, nil)
}

func (thisTest *loginTest) WhenLoginGetRequestDone() {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/login"), nil)
	r.ServeHTTP(thisTest.responseRecorder, req)
}

func (thisTest *loginTest) WhenLoginPostRequestDone(username, password string) {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()

	body := fmt.Sprintf("username=%s&password=%s", username, password)
	req, _ := http.NewRequest("POST", fmt.Sprintf("/login"), strings.NewReader(body))
	req.Header.Add("Content-type", "application/x-www-form-urlencoded")
	r.ServeHTTP(thisTest.responseRecorder, req)
}

func (thisTest *loginTest) WhenLogoutRequestDone(sessionCookie string) {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/logout"), nil)
	req.Header.Add("Cookie", sessionCookie)

	r.ServeHTTP(thisTest.responseRecorder, req)
}

func (thisTest *loginTest) WhenAlbumListRequestDoneWithoutCookie() {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/albums"), nil)

	r.ServeHTTP(thisTest.responseRecorder, req)
}

func (thisTest *loginTest) WhenAlbumListRequestDoneWithSessionCookie(sessionCookie string) {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/albums"), nil)
	req.Header.Add("Cookie", sessionCookie)

	r.ServeHTTP(thisTest.responseRecorder, req)
}

func (thisTest *loginTest) ThenLoginFormShown() {
	responseSBody := thisTest.extractResponseBody()
	thisTest.Assert.Contains(responseSBody, "<form action=\"/login\" method=\"post\">")
}

func (thisTest *loginTest) ThenSessionCookieSet() string {
	setCookieHeader := thisTest.ExtractOnlyOneHeader("Set-Cookie")

	sessionCookieRegex := regexp.MustCompile(sessionCookieRegexStr)
	thisTest.Assert.Regexp(sessionCookieRegex, setCookieHeader)
	sessionCookieWithSemicolon := sessionCookieRegex.FindString(setCookieHeader)
	return sessionCookieWithSemicolon[:len(sessionCookieWithSemicolon)-1]
}

func TestLoginFormShown(t *testing.T) {
	test := newLoginTest(t)
	defer test.Finish()

	test.WhenLoginGetRequestDone()
	test.ThenResponseSuccess()
	test.ThenLoginFormShown()
}

func TestLoginSuccess(t *testing.T) {
	test := newLoginTest(t)
	defer test.Finish()

	test.GivenPasswordCorrect("test_user", "test_pass")
	test.WhenLoginPostRequestDone("test_user", "test_pass")
	test.ThenResponseIsMovedPermanently()
	test.ThenRedirectedTo("/albums")

	sessionCookie := test.ThenSessionCookieSet()
	test.WhenLogoutRequestDone(sessionCookie)
	test.ThenResponseIsFound()
	test.ThenRedirectedTo("/login")
}

func TestIncorrectUserPassword(t *testing.T) {
	test := newLoginTest(t)
	defer test.Finish()

	test.GivenPasswordInCorrect("test_user", "test_pass")
	test.WhenLoginPostRequestDone("test_user", "test_pass")
	test.ThenResponseIsUnauthorized()
}

func TestAlbumListNotDisplayedWithoutLogin(t *testing.T) {
	test := newLoginTest(t)
	defer test.Finish()

	test.WhenAlbumListRequestDoneWithoutCookie()
	test.ThenResponseIsMovedPermanently()
	test.ThenRedirectedTo("/login")
}

func TestAlbumListDisplayedAfterLogin(t *testing.T) {
	test := newLoginTest(t)
	defer test.Finish()

	test.GivenPasswordCorrect("test_user", "test_pass")
	test.GivenEmptyAlbumList()
	test.WhenLoginPostRequestDone("test_user", "test_pass")
	test.ThenResponseIsMovedPermanently()
	test.ThenRedirectedTo("/albums")

	sessionCookie := test.ThenSessionCookieSet()

	test.WhenAlbumListRequestDoneWithSessionCookie(sessionCookie)
	test.ThenResponseSuccess()
}

func TestRegexp(t *testing.T) {
	str := "session=MTY1ODI0NzI2OHxEdi1CQkFFQ180SUFBUkFCRUFBQUpfLUNBQUVHYzNSeWFXNW5EQVlBQkhWelpYSUdjM1J5YVc1bkR" +
		"Bc0FDWFJsYzNSZmRYTmxjZz09fE3UzPiDI8-AdwFTm_8I00M1f04pIs8AF3GC22ayo-14; Path=/; Expires=Thu, 18 Aug 2022 16:14:28 GMT; Max-Age=2592000"
	sessionCookieRegex := regexp.MustCompile(sessionCookieRegexStr)
	assert.Regexp(t, sessionCookieRegex, str)
	sessionCookieWithSemicolon := sessionCookieRegex.FindString(str)
	assert.Equal(
		t,
		sessionCookieWithSemicolon[:len(sessionCookieWithSemicolon)-1],
		"session=MTY1ODI0NzI2OHxEdi1CQkFFQ180SUFBUkFCRUFBQUpfLUNBQUVHYzNSeWFXNW5EQVlBQkhWelpYSUdjM1J5YVc1bkR"+
			"Bc0FDWFJsYzNSZmRYTmxjZz09fE3UzPiDI8-AdwFTm_8I00M1f04pIs8AF3GC22ayo-14")
}
