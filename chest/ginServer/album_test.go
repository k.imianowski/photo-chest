package ginServer_test

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"imiano.pl/chest/globals"
	"imiano.pl/chest/mock_awsIntegration"
	"imiano.pl/chest/mock_controller"
	"imiano.pl/chest/model"
)

type AlbumTest struct {
	ginServerTest
	testAlbumData model.Album
}

func newAlbumTest(t *testing.T) *AlbumTest {
	mockController := gomock.NewController(t)

	return &AlbumTest{
		ginServerTest: ginServerTest{
			Assert:              assert.New(t),
			mockController:      mockController,
			mockAws:             mock_awsIntegration.NewMockAwsIntegration(mockController),
			mockPasswordManager: mock_controller.NewMockPasswordManager(mockController),
			mockTokenManager:    mock_controller.NewMockTokenManager(mockController),
		},
		testAlbumData: model.Album{
			BucketSubdirName: testAlbum,
			Name:             testAlbumName,
			Password:         testAlbumPass,
			Description:      testAlbumDesc,
		},
	}
}

func (test *AlbumTest) GivenAlbumDoesNotExist(album string) {
	test.mockAws.EXPECT().GetAlbum(album).Return(nil, fmt.Errorf("Some error"))
}

func (test *AlbumTest) GivenAlbumFound(album string) {
	test.mockAws.EXPECT().GetAlbum(album).Return(
		&model.Album{
			BucketSubdirName: testAlbum,
			Name:             testAlbumName,
			Password:         testAlbumPass,
			Description:      testAlbumDesc,
		},
		nil)
}

func (test *AlbumTest) GivenOneImageFound(album, image string) {
	test.mockAws.EXPECT().ListImagesInAlbum(globals.Get().ResizedPhotosBucketName, album).Return([]string{image}, nil)
}

func (test *AlbumTest) GivenTokenGenerated(album, token string) {
	test.mockTokenManager.EXPECT().GenerateTokenForAlbum(album).Return(token, nil)
}

func (test *AlbumTest) WhenRequestDoneWithoutPassword(album string) {
	test.responseRecorder = httptest.NewRecorder()

	r := test.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/album/show/%s", album), nil)
	r.ServeHTTP(test.responseRecorder, req)
}

func (test *AlbumTest) WhenRequestDone(album, password string) {
	test.responseRecorder = httptest.NewRecorder()

	r := test.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/album/show/%s?password=%s", album, password), nil)
	r.ServeHTTP(test.responseRecorder, req)
}

func (test *AlbumTest) ThenPasswordFormShown(album string) {
	test.ThenResponseBodyContains([]string{
		fmt.Sprintf("<form action=\"/album/show/%s\" method=\"get\">", album),
	})
}

func (test *AlbumTest) ThenImageListedWithToken(album, image, token string) {
	test.ThenResponseBodyContains([]string{
		fmt.Sprintf("href=\"/photo/%s/%s?accessToken=%s\"", album, image, token),
		fmt.Sprintf("img src=\"/thumbnail/%s/%s?accessToken=%s\"", album, image, token),
	})
}

func TestErrorWhenAlbumDoesNotExist(t *testing.T) {
	test := newAlbumTest(t)
	defer test.Finish()

	test.GivenAlbumDoesNotExist(testAlbum)
	test.WhenRequestDoneWithoutPassword(testAlbum)
	test.ThenResponseInternalError()
}

func TestPasswordFormDisplayedWithoutPassword(t *testing.T) {
	test := newAlbumTest(t)
	defer test.Finish()

	test.GivenAlbumFound(testAlbum)
	test.WhenRequestDoneWithoutPassword(testAlbum)
	test.ThenResponseSuccess()
	test.ThenPasswordFormShown(testAlbum)
}

func TestUnauthorizedWithWrongPassword(t *testing.T) {
	test := newAlbumTest(t)
	defer test.Finish()

	test.GivenAlbumFound(testAlbum)
	test.WhenRequestDone(testAlbum, "some other password")
	test.ThenResponseIsUnauthorized()
	test.ThenPasswordFormShown(testAlbum)
}

func TestImagesListedWithToken(t *testing.T) {
	test := newAlbumTest(t)
	defer test.Finish()

	test.GivenAlbumFound(testAlbum)
	test.GivenOneImageFound(testAlbum, testImage)
	log.Printf("Got here1")
	test.GivenTokenGenerated(testAlbum, testToken)
	log.Printf("Got here3")
	test.WhenRequestDone(testAlbum, testAlbumPass)

	log.Printf("Got here2")
	test.ThenResponseSuccess()
	test.ThenImageListedWithToken(testAlbum, testImage, testToken)
}
