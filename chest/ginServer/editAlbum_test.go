package ginServer_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"imiano.pl/chest/mock_controller"
	"imiano.pl/chest/model"
)

type editAlbumTest struct {
	testWithAuthMock
	album *model.Album
}

func newEditAlbumTest(t *testing.T) *editAlbumTest {
	mockController := gomock.NewController(t)
	authRequiredMock := mock_controller.NewMockAuthRequired(mockController)
	authRequiredMock.EXPECT().Handler(gomock.Any()).AnyTimes().Do(func(c *gin.Context) { c.Next() })

	return &editAlbumTest{
		testWithAuthMock: *newTestWithAuthMock(t),
	}
}

func (thisTest *editAlbumTest) GivenAlbumFound(album string) {
	thisTest.album = &model.Album{
		BucketSubdirName: album,
		Name:             album + "name",
		Password:         album + "password",
		Description:      album + "description",
	}
	thisTest.mockAws.EXPECT().GetAlbum(album).Return(thisTest.album, nil)
}

func (thisTest *editAlbumTest) GivenAlbumSaved(album string) {
	thisTest.album = &model.Album{
		BucketSubdirName: album,
		Name:             album + "name",
		Password:         album + "password",
		Description:      album + "description",
	}
	thisTest.mockAws.EXPECT().SaveAlbum(thisTest.album).Return(nil)
}

func (thisTest *editAlbumTest) GivenAlbumNotFound(album string) {
	thisTest.mockAws.EXPECT().GetAlbum(album).Return(nil, fmt.Errorf("Some error"))
}

func (thisTest *editAlbumTest) WhenEditAlbumGetRequestDone(album string) {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/album/edit/%s", album), nil)

	r.ServeHTTP(thisTest.responseRecorder, req)
}

func (thisTest *editAlbumTest) WhenAddAlbumGetRequestDone() {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/album/add"), nil)

	r.ServeHTTP(thisTest.responseRecorder, req)
}

func (thisTest *editAlbumTest) WhenSaveAlbumRequestDone(album string) {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	body := fmt.Sprintf("BucketSubdirName=%s&Name=%s&Password=%s&Description=%s", album, album+"name", album+"password", album+"description")
	req, _ := http.NewRequest("POST", "/album/save", strings.NewReader(body))
	req.Header.Add("Content-type", "application/x-www-form-urlencoded")

	r.ServeHTTP(thisTest.responseRecorder, req)
}

func (thisTest *editAlbumTest) WhenSaveAlbumRequestDoneWithEmptyPassword(album string) {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	body := fmt.Sprintf("BucketSubdirName=%s&Name=%s&Password=%s&Description=%s", album, album+"name", "", album+"description")
	req, _ := http.NewRequest("POST", "/album/save", strings.NewReader(body))
	req.Header.Add("Content-type", "application/x-www-form-urlencoded")

	r.ServeHTTP(thisTest.responseRecorder, req)
}

func TestAlbumEditFormShown(t *testing.T) {
	test := newEditAlbumTest(t)
	defer test.Finish()

	testAlbum := "testAlbumAAA"

	test.GivenAlbumFound(testAlbum)
	test.WhenEditAlbumGetRequestDone(testAlbum)
	test.ThenResponseSuccess()
	test.ThenResponseBodyContains([]string{testAlbum + "name", testAlbum + "password"})
}

func TestErrorReturnedIfAlbumNotFound(t *testing.T) {
	test := newEditAlbumTest(t)
	defer test.Finish()

	testAlbum := "testAlbumAAA"

	test.GivenAlbumNotFound(testAlbum)
	test.WhenEditAlbumGetRequestDone(testAlbum)
	test.ThenResponseInternalError()
}

func TestAlbumSaved(t *testing.T) {
	test := newEditAlbumTest(t)
	defer test.Finish()

	testAlbum := "testAlbumAAA"

	test.GivenAlbumSaved(testAlbum)
	test.WhenSaveAlbumRequestDone(testAlbum)
	test.ThenResponseIsMovedPermanently()
	test.ThenRedirectedTo("/albums")
}

func TestAlbumNotSavedWithoutPassword(t *testing.T) {
	test := newEditAlbumTest(t)
	defer test.Finish()

	testAlbum := "testAlbumAAA"

	test.WhenSaveAlbumRequestDoneWithEmptyPassword(testAlbum)
	test.ThenResponseInternalError()
}

func TestEmptyFormShownOnAddRequest(t *testing.T) {
	test := newEditAlbumTest(t)
	defer test.Finish()

	test.WhenAddAlbumGetRequestDone()
	test.ThenResponseSuccess()
	test.ThenResponseBodyContains([]string{
		"name=\"BucketSubdirName\" value=\"\" >",
	})
}
