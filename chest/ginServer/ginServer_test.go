package ginServer_test

import (
	"io/ioutil"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"imiano.pl/chest/controller"
	"imiano.pl/chest/ginServer"
	"imiano.pl/chest/mock_awsIntegration"
	"imiano.pl/chest/mock_controller"
)

const testAlbum = "testAlbumAAA"
const testImage = "testImageAAA.jpg"
const testToken = "testTokenAAA"
const testPassword = "testPasswordAAA"
const testAlbumName = "testAlbumNameAAA"
const testAlbumDesc = "testAlbumDescAAA"
const testAlbumPass = "testAlbumPassAAA"

type ginServerTest struct {
	Assert              *assert.Assertions
	mockController      *gomock.Controller
	mockAws             *mock_awsIntegration.MockAwsIntegration
	responseRecorder    *httptest.ResponseRecorder
	mockPasswordManager *mock_controller.MockPasswordManager
	mockTokenManager    *mock_controller.MockTokenManager
}

func (thisTest *ginServerTest) Finish() {
	thisTest.mockController.Finish()
}

func (thisTest *ginServerTest) ThenResponseSuccess() {
	thisTest.Assert.Equal(200, thisTest.responseRecorder.Code)
}

func (thisTest *ginServerTest) ThenResponseInternalError() {
	thisTest.Assert.Equal(500, thisTest.responseRecorder.Code)
}

func (thisTest *ginServerTest) ThenResponseIsMovedPermanently() {
	thisTest.Assert.Equal(301, thisTest.responseRecorder.Code)
}

func (thisTest *ginServerTest) ThenResponseIsFound() {
	thisTest.Assert.Equal(302, thisTest.responseRecorder.Code)
}

func (thisTest *ginServerTest) ThenResponseIsUnauthorized() {
	thisTest.Assert.Equal(401, thisTest.responseRecorder.Code)
}

func (thisTest *ginServerTest) ThenRedirectedTo(expectedLocation string) {
	locationHeader := thisTest.ExtractOnlyOneHeader("Location")
	thisTest.Assert.Equal(expectedLocation, locationHeader)
}

func (thisTest *ginServerTest) ExtractOnlyOneHeader(key string) string {
	headers := thisTest.responseRecorder.Result().Header[key]
	thisTest.Assert.Equal(1, len(headers))
	return headers[0]
}

func (thisTest *ginServerTest) extractResponseBody() string {
	responseData, _ := ioutil.ReadAll(thisTest.responseRecorder.Body)
	return string(responseData)
}

func (thisTest *ginServerTest) ThenResponseBodyContains(contents []string) {
	responseBody := thisTest.extractResponseBody()
	for _, c := range contents {
		thisTest.Assert.Contains(responseBody, c)
	}
}

func (thisTest *ginServerTest) CreateTestRouter() *gin.Engine {
	controllerDeps := controller.ControllerDependencies{
		Aws:          thisTest.mockAws,
		PassManager:  thisTest.mockPasswordManager,
		TokenManager: thisTest.mockTokenManager,
	}
	deps := &ginServer.RouterDependencies{
		Ctrl:          controller.NewController(controllerDeps),
		TemplatesPath: "../templates/*",
		AuthRequired:  controller.NewAuthRequiredImpl(),
	}
	return ginServer.CreateRouter(deps)
}
