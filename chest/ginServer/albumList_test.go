package ginServer_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"imiano.pl/chest/controller"
	"imiano.pl/chest/ginServer"
	"imiano.pl/chest/mock_controller"
	"imiano.pl/chest/model"
)

type albumListTest struct {
	testWithAuthMock
}

func newAlbumListTest(t *testing.T) *albumListTest {
	mockController := gomock.NewController(t)
	authRequiredMock := mock_controller.NewMockAuthRequired(mockController)
	authRequiredMock.EXPECT().Handler(gomock.Any()).AnyTimes().Do(func(c *gin.Context) { c.Next() })

	return &albumListTest{
		testWithAuthMock: *newTestWithAuthMock(t),
	}
}

func (thisTest *albumListTest) CreateTestRouter() *gin.Engine {
	controllerDeps := controller.ControllerDependencies{
		Aws:          thisTest.mockAws,
		PassManager:  thisTest.mockPasswordManager,
		TokenManager: thisTest.mockTokenManager,
	}
	deps := &ginServer.RouterDependencies{
		Ctrl:          controller.NewController(controllerDeps),
		TemplatesPath: "../templates/*",
		AuthRequired:  thisTest.authRequiredMock,
	}
	return ginServer.CreateRouter(deps)
}

func (thisTest *albumListTest) GivenEmptyAlbumList() {
	thisTest.mockAws.EXPECT().ListAlbums().Return([]model.Album{}, nil)
}

func (thisTest *albumListTest) GivenAlbumList(albumNames []string) {
	albums := []model.Album{}
	for _, albumName := range albumNames {
		albums = append(albums, model.Album{
			BucketSubdirName: "subdir" + albumName,
			Name:             "name" + albumName,
			Password:         "password" + albumName,
			Description:      "description" + albumName,
		})
	}
	thisTest.mockAws.EXPECT().ListAlbums().Return(albums, nil)
}

func (thisTest *albumListTest) GivenAlbumListRetrievalFails() {
	thisTest.mockAws.EXPECT().ListAlbums().Return([]model.Album{}, fmt.Errorf("some error"))
}

func (thisTest *albumListTest) WhenAlbumListRequestDone() {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/albums"), nil)

	r.ServeHTTP(thisTest.responseRecorder, req)
}

func TestEmptyAlbumList(t *testing.T) {
	test := newAlbumListTest(t)
	defer test.Finish()

	test.GivenEmptyAlbumList()
	test.WhenAlbumListRequestDone()

	test.ThenResponseSuccess()
}

func TestAlbumListRetrieveFailure(t *testing.T) {
	test := newAlbumListTest(t)
	defer test.Finish()

	test.GivenAlbumListRetrievalFails()
	test.WhenAlbumListRequestDone()

	test.ThenResponseInternalError()
}

func TestAlbumListRendered(t *testing.T) {
	test := newAlbumListTest(t)
	defer test.Finish()

	test.GivenAlbumList([]string{"testAlbumAAA", "testAlbumBBB"})
	test.WhenAlbumListRequestDone()

	test.ThenResponseSuccess()
	test.ThenResponseBodyContains([]string{"subdirtestAlbumAAA", "nametestAlbumAAA", "passwordtestAlbumAAA", "testAlbumBBB"})
}
