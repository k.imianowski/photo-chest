package ginServer_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"imiano.pl/chest/globals"
	"imiano.pl/chest/mock_awsIntegration"
	"imiano.pl/chest/mock_controller"
	"imiano.pl/chest/model"
)

type testShowPhoto struct {
	testImage *model.Image
	ginServerTest
}

func newTestShowPhoto(t *testing.T) *testShowPhoto {
	mockController := gomock.NewController(t)

	return &testShowPhoto{
		ginServerTest: ginServerTest{
			Assert:           assert.New(t),
			mockController:   mockController,
			mockAws:          mock_awsIntegration.NewMockAwsIntegration(mockController),
			mockTokenManager: mock_controller.NewMockTokenManager(mockController),
		},
	}
}

func (thisTest *testShowPhoto) GivenImageFound(album, photo string) {
	thisTest.testImage = &model.Image{
		N:      3,
		Reader: strings.NewReader("abc"),
	}
	imgPath := fmt.Sprintf("resized/%s/%s", album, photo)
	thisTest.mockAws.EXPECT().GetImg(globals.Get().ResizedPhotosBucketName, imgPath).Return(thisTest.testImage, nil).Times(1)
}

func (thisTest *testShowPhoto) WhenRequestDone(album, photo, token string) {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/photo/%s/%s?accessToken=%s", album, photo, token), nil)
	r.ServeHTTP(thisTest.responseRecorder, req)
}

func (thisTest *testShowPhoto) GivenTokenValidationFails(token, album string) {
	thisTest.mockTokenManager.EXPECT().IsTokenValidForAlbum(token, album).Return(false)
}

func (thisTest *testShowPhoto) GivenTokenValid(token, album string) {
	thisTest.mockTokenManager.EXPECT().IsTokenValidForAlbum(token, album).Return(true)
}

func (thisTest *testShowPhoto) WhenRequestDoneWithoutToken(album, photo string) {
	thisTest.responseRecorder = httptest.NewRecorder()

	r := thisTest.CreateTestRouter()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/photo/%s/%s", album, photo), nil)
	r.ServeHTTP(thisTest.responseRecorder, req)
}

func TestImageShown(t *testing.T) {
	test := newTestShowPhoto(t)
	defer test.Finish()

	test.GivenTokenValid(testToken, testAlbum)
	test.GivenImageFound(testAlbum, testImage)
	test.WhenRequestDone(testAlbum, testImage, testToken)
	test.ThenResponseSuccess()
}

func TestImageUnauthorizedWithoutToken(t *testing.T) {
	test := newTestShowPhoto(t)
	defer test.Finish()

	test.WhenRequestDoneWithoutToken(testAlbum, testImage)
	test.ThenResponseIsUnauthorized()
}

func TestImageUnauthorizedWhenTokenValidationFails(t *testing.T) {
	test := newTestShowPhoto(t)
	defer test.Finish()

	test.GivenTokenValidationFails(testToken, testAlbum)
	test.WhenRequestDone(testAlbum, testImage, testToken)
	test.ThenResponseIsUnauthorized()
}
