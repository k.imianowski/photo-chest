package ginServer

import (
	"log"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"imiano.pl/chest/awsIntegration"
	"imiano.pl/chest/controller"
	"imiano.pl/chest/globals"
	"imiano.pl/chest/utils"
)

type RouterDependencies struct {
	Ctrl          *controller.Controller
	TemplatesPath string
	AuthRequired  controller.AuthRequired
}

func CreateProductionRouter() *gin.Engine {
	prodAwsIntegration := awsIntegration.NewRealAwsIntegration()
	controllerDependencies := controller.ControllerDependencies{
		Aws:         prodAwsIntegration,
		PassManager: controller.NewPasswordManager(prodAwsIntegration),
		TokenManager: controller.NewTokenManagerImpl(
			globals.Get().TokenExpiration,
			&utils.RealTimeProvider{},
		),
	}
	dependencies := &RouterDependencies{
		Ctrl:          controller.NewController(controllerDependencies),
		TemplatesPath: globals.Get().TemplatesPath,
		AuthRequired:  controller.NewAuthRequiredImpl(),
	}
	return CreateRouter(dependencies)
}

func CreateRouter(deps *RouterDependencies) *gin.Engine {
	pathPrefix := globals.Get().PathPrefix
	log.Printf("Starting server with path prefix %s", pathPrefix)
	r := gin.Default()

	r.LoadHTMLGlob(deps.TemplatesPath)
	r.Use(sessions.Sessions("session", cookie.NewStore(globals.Get().Secret)))

	r.GET(pathPrefix+"/photo/:album/:photo", func(c *gin.Context) {
		deps.Ctrl.ShowPhoto("resized", c)
	})
	r.GET(pathPrefix+"/thumbnail/:album/:photo", func(c *gin.Context) {
		deps.Ctrl.ShowPhoto("thumbnail", c)
	})
	r.GET(pathPrefix+"/album/show/:album", deps.Ctrl.ShowAlbum)
	r.GET(pathPrefix+"/", deps.Ctrl.MainPage)
	r.GET(pathPrefix+"/login", deps.Ctrl.LoginGetHandler)
	r.POST(pathPrefix+"/login", deps.Ctrl.LoginPostHandler)
	r.GET(pathPrefix+"/logout", deps.Ctrl.Logout)

	protectedRoutes := r.Group("/")
	protectedRoutes.Use(deps.AuthRequired.Handler)
	protectedRoutes.GET(pathPrefix+"/albums", deps.Ctrl.AlbumList)
	protectedRoutes.GET(pathPrefix+"/album/add", deps.Ctrl.AddAlbum)
	protectedRoutes.GET(pathPrefix+"/album/edit/:album", deps.Ctrl.EditAlbum)
	protectedRoutes.POST(pathPrefix+"/album/save", deps.Ctrl.SaveAlbum)
	return r
}
