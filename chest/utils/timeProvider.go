package utils

import "time"

type TimeProvider interface {
	Now() time.Time
}

type RealTimeProvider struct{}

func (t *RealTimeProvider) Now() time.Time {
	return time.Now()
}

type AdjustableTimeProvider struct {
	reportedTime time.Time
}

func NewAdjustableTimeProvider() *AdjustableTimeProvider {
	return &AdjustableTimeProvider{
		reportedTime: time.Now(),
	}
}

func (t *AdjustableTimeProvider) Now() time.Time {
	return t.reportedTime
}

func (t *AdjustableTimeProvider) AdvanceTime(duration time.Duration) {
	t.reportedTime = t.reportedTime.Add(duration)
}
