package main

import (
	"log"

	"imiano.pl/chest/ginServer"
)

func main() {
	log.Printf("Starting local server")
	r := ginServer.CreateProductionRouter()
	r.Run()
}
