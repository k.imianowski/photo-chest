package controller_test

import (
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"imiano.pl/chest/controller"
	"imiano.pl/chest/utils"
)

type imageAccessTokenTest struct {
	timeProvider   *utils.AdjustableTimeProvider
	Assert         *assert.Assertions
	mockController *gomock.Controller
	tokenManager   *controller.TokenManagerImpl
}

func newTest(t *testing.T) *imageAccessTokenTest {
	mockController := gomock.NewController(t)
	timeProvider := utils.NewAdjustableTimeProvider()
	return &imageAccessTokenTest{
		Assert:         assert.New(t),
		mockController: mockController,
		timeProvider:   timeProvider,
		tokenManager:   controller.NewTokenManagerImpl("15m", timeProvider),
	}
}

func (test imageAccessTokenTest) Finish() {
	test.mockController.Finish()
}

func TestTokenGeneratedIsValid(t *testing.T) {
	test := newTest(t)
	defer test.Finish()
	testAlbum := "testAlbumAAAA"

	token, err := test.tokenManager.GenerateTokenForAlbum(testAlbum)
	test.Assert.NotEmpty(token)
	test.Assert.Nil(err)

	isValid := test.tokenManager.IsTokenValidForAlbum(token, testAlbum)
	test.Assert.True(isValid)
}

func TestTokenGeneratedExpiresAfter15minutes(t *testing.T) {
	test := newTest(t)
	defer test.Finish()
	testAlbum := "testAlbumAAAA"

	token, err := test.tokenManager.GenerateTokenForAlbum(testAlbum)
	test.Assert.NotEmpty(token)
	test.Assert.Nil(err)

	test.Assert.True(test.tokenManager.IsTokenValidForAlbum(token, testAlbum))
	duration14m, _ := time.ParseDuration("14m")
	test.timeProvider.AdvanceTime(duration14m)
	test.Assert.True(test.tokenManager.IsTokenValidForAlbum(token, testAlbum))
	duration2m, _ := time.ParseDuration(("2m"))
	test.timeProvider.AdvanceTime(duration2m)
	test.Assert.False(test.tokenManager.IsTokenValidForAlbum(token, testAlbum))
}

func TestTokenIsNotAcceptedForADifferentAlbum(t *testing.T) {
	test := newTest(t)
	defer test.Finish()
	testAlbum := "testAlbumAAAA"

	token, err := test.tokenManager.GenerateTokenForAlbum(testAlbum)
	test.Assert.NotEmpty(token)
	test.Assert.Nil(err)

	isValid := test.tokenManager.IsTokenValidForAlbum(token, "someOtherAlbum")
	test.Assert.False(isValid)
}

func TestTokenIsNotAcceptedWhenChanged(t *testing.T) {
	test := newTest(t)
	defer test.Finish()
	testAlbum := "testAlbumAAAA"

	token, err := test.tokenManager.GenerateTokenForAlbum(testAlbum)
	test.Assert.NotEmpty(token)
	test.Assert.Nil(err)

	isValid := test.tokenManager.IsTokenValidForAlbum(token+"aaa", testAlbum)
	test.Assert.False(isValid)
}
