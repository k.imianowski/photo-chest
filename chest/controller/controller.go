package controller

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"imiano.pl/chest/awsIntegration"
	"imiano.pl/chest/globals"
	"imiano.pl/chest/model"
)

type ControllerDependencies struct {
	Aws          awsIntegration.AwsIntegration
	PassManager  PasswordManager
	TokenManager TokenManager
}

type Controller struct {
	Aws             awsIntegration.AwsIntegration
	passwordManager PasswordManager
	tokenManager    TokenManager
	pathPrefix      string
}

func NewController(deps ControllerDependencies) *Controller {
	return &Controller{
		Aws:             deps.Aws,
		passwordManager: deps.PassManager,
		tokenManager:    deps.TokenManager,
		pathPrefix:      globals.Get().PathPrefix,
	}
}

func (ctrl *Controller) MainPage(c *gin.Context) {
	c.HTML(http.StatusOK, "hello.html", getParams("Photo chest", c))
}

func (ctrl *Controller) ShowPhoto(size string, c *gin.Context) {
	album := c.Param("album")
	photo := c.Param("photo")
	token := c.Query("accessToken")
	if token == "" {
		log.Printf("Access token empty")
		c.Status(http.StatusUnauthorized)
		return
	}
	if !ctrl.tokenManager.IsTokenValidForAlbum(token, album) {
		log.Printf("Incorrect access token for album %s", album)
		c.Status(http.StatusUnauthorized)
		return
	}
	log.Printf("Showing photo %s %s %s", size, album, photo)
	image, err := ctrl.Aws.GetImg(globals.Get().ResizedPhotosBucketName, fmt.Sprintf("%s/%s/%s", size, album, photo))
	if err != nil {
		log.Printf("aws error: %v", err)
		c.Status(http.StatusInternalServerError)
		return
	}

	contentType := "image/jpeg"

	extraHeaders := map[string]string{
		"Content-Disposition": fmt.Sprintf("filename=\"%s\"", photo),
	}

	c.DataFromReader(http.StatusOK, image.N, contentType, image.Reader, extraHeaders)
}

func getParamsWith(title string, c *gin.Context, h gin.H) gin.H {
	h["title"] = title
	pathPrefix := globals.Get().PathPrefix
	h["pathPrefix"] = pathPrefix

	session := sessions.Default(c)
	user := session.Get("user")
	h["user_logged_in"] = false
	if user != nil {
		if v, ok := user.(string); ok {
			h["user_logged_in"] = true
			h["username"] = v
		}
	}
	return h
}

func getParams(title string, c *gin.Context) gin.H {
	return getParamsWith(title, c, make(gin.H))
}

func (ctrl *Controller) ShowAlbum(c *gin.Context) {
	albumId := c.Param("album")
	password := c.Query("password")
	log.Printf("Got password: %v", password)
	album, err := ctrl.Aws.GetAlbum(albumId)
	if err != nil {
		log.Printf("Error loading album data %s", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	if password == "" {
		c.HTML(http.StatusOK, "album_password.html", getParamsWith(fmt.Sprintf("Album %s", album.Name), c, gin.H{"album": album}))
		return
	}
	log.Printf("Will verify password: %v %v", password, album.Password)
	if password != album.Password {
		c.HTML(http.StatusUnauthorized, "album_password.html", getParamsWith(fmt.Sprintf("Album %s", album.Name), c, gin.H{"album": album}))
		return
	}
	log.Printf("Generating token")
	token, err := ctrl.tokenManager.GenerateTokenForAlbum(albumId)
	if err != nil {
		log.Printf("error generating token: %v", err)
		c.Status(http.StatusInternalServerError)
		return
	}
	log.Printf("Token generated: %s", token)
	photos, err := ctrl.Aws.ListImagesInAlbum(globals.Get().ResizedPhotosBucketName, albumId)
	if err != nil {
		log.Printf("aws error: %v", err)
		c.Status(http.StatusInternalServerError)
		return
	}
	c.HTML(http.StatusOK, "album.html", getParamsWith(fmt.Sprintf("Album %s", album.Name), c, gin.H{
		"album":       album,
		"photos":      photos,
		"accessToken": token,
	}))
}

func (ctrl *Controller) LoginGetHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "login.html", getParams("Login", c))
}

func (ctrl *Controller) AlbumList(c *gin.Context) {
	log.Printf("Will show album list")
	albums, err := ctrl.Aws.ListAlbums()
	if err != nil {
		log.Printf("Error loading album list %s", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	log.Printf("Got %d albums", len(albums))

	c.HTML(http.StatusOK, "album_list.html", getParamsWith(fmt.Sprintf("All albums"), c, gin.H{"albums": albums}))
}

func (ctrl *Controller) LoginPostHandler(c *gin.Context) {
	session := sessions.Default(c)

	username := c.PostForm("username")
	password := c.PostForm("password")
	if len(username) == 0 || len(password) == 0 {
		log.Printf("Password or login empty %s %s", username, password)
		c.HTML(http.StatusBadRequest, "login.html", getParams("Login", c))
		return
	}
	if !ctrl.passwordManager.CheckPassword(username, password) {
		log.Printf("Wrong username/password")
		c.HTML(http.StatusUnauthorized, "login.html", getParams("Login", c))
		return
	}

	session.Set("user", username)
	if err := session.Save(); err != nil {
		log.Printf("Error setting session")
		c.HTML(http.StatusInternalServerError, "login.html", getParams("Login", c))
		return
	}
	log.Printf("Logged in user %s", username)
	c.Redirect(http.StatusMovedPermanently, ctrl.pathPrefix+"/albums")
}

func (ctrl *Controller) Logout(c *gin.Context) {
	log.Println("Will log out")
	session := sessions.Default(c)
	session.Delete("user")
	if err := session.Save(); err != nil {
		log.Println("Failed to save session:", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	log.Println("User logged out")

	c.Redirect(http.StatusFound, ctrl.pathPrefix+"/login")
}

func (ctrl *Controller) EditAlbum(c *gin.Context) {
	album := c.Param("album")
	log.Printf("Got request to edit album %s", album)
	albumData, err := ctrl.Aws.GetAlbum(album)
	if err != nil {
		log.Println("Failed to get album data:", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	log.Printf("Album data is %v", albumData)

	c.HTML(http.StatusOK, "edit_album.html", getParamsWith("Edit album", c, gin.H{
		"album": albumData,
	}))
}

func (ctrl *Controller) SaveAlbum(c *gin.Context) {
	album := &model.Album{
		BucketSubdirName: c.PostForm("BucketSubdirName"),
		Name:             c.PostForm("Name"),
		Password:         c.PostForm("Password"),
		Description:      c.PostForm("Description"),
	}
	for _, param := range []string{album.BucketSubdirName, album.Name, album.Password, album.Description} {
		log.Printf("Got param %s", param)
		if param == "" {
			c.AbortWithError(http.StatusInternalServerError, fmt.Errorf("Empty parameter"))
			return
		}
	}
	err := ctrl.Aws.SaveAlbum(album)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.Redirect(http.StatusMovedPermanently, ctrl.pathPrefix+"/albums")
}

func (ctrl *Controller) AddAlbum(c *gin.Context) {
	album := &model.Album{}

	c.HTML(http.StatusOK, "edit_album.html", getParamsWith("Edit album", c, gin.H{
		"album": album,
	}))
}
