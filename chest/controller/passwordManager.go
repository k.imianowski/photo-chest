package controller

import (
	"crypto/md5"
	"encoding/base64"
	"fmt"
	"log"

	"imiano.pl/chest/awsIntegration"
)

type PasswordManager interface {
	CheckPassword(username, password string) bool
}

type PasswordManagerImpl struct {
	aws awsIntegration.AwsIntegration
}

func NewPasswordManager(aws awsIntegration.AwsIntegration) PasswordManager {
	return &PasswordManagerImpl{
		aws: aws,
	}
}

func (pm *PasswordManagerImpl) CheckPassword(username, password string) bool {
	credentials, error := pm.aws.GetCredentials(username)
	if error != nil {
		log.Printf("Failed to get credentials for user %s", username)
		return false
	}
	hashInput := fmt.Sprintf("%s;%s;%s", password, credentials.Pepper, "imiano_chest")
	md5Sum := md5.New().Sum([]byte(hashInput))
	b64Encoded := base64.StdEncoding.EncodeToString(md5Sum)

	log.Printf("For user %s and passwor %s got expected hash %s", username, password, b64Encoded)
	return b64Encoded == credentials.PassHash
}
