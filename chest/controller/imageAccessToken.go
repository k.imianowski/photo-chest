package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/golang-jwt/jwt"
	"imiano.pl/chest/globals"
	"imiano.pl/chest/utils"
)

type TokenManager interface {
	GenerateTokenForAlbum(album string) (string, error)
	IsTokenValidForAlbum(token, album string) bool
}

type TokenManagerImpl struct {
	expirationDuration time.Duration
	timeProvider       utils.TimeProvider
}

func NewTokenManagerImpl(tokenExpirationDuration string, timeProvider utils.TimeProvider) *TokenManagerImpl {
	expirationDuration, err := time.ParseDuration(tokenExpirationDuration)
	if err != nil {
		panic(fmt.Sprintf("Error parsing token expiration timeout value of %s", tokenExpirationDuration))
	}
	return &TokenManagerImpl{
		timeProvider:       timeProvider,
		expirationDuration: expirationDuration,
	}
}

func (tokenManager *TokenManagerImpl) GenerateTokenForAlbum(album string) (string, error) {
	expireTime := tokenManager.timeProvider.Now().Add(tokenManager.expirationDuration)
	log.Printf("Creating token with expiration time %v", expireTime)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"album":  album,
		"expire": expireTime.Unix(),
	})
	tokenStr, err := token.SignedString(globals.Get().Secret)
	return tokenStr, err
}

func (tokenManager *TokenManagerImpl) IsTokenValidForAlbum(token, album string) bool {

	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return globals.Get().Secret, nil
	})

	if err != nil {
		log.Printf("Error parsing token %v", err)
		return false
	}
	if claims, ok := parsedToken.Claims.(jwt.MapClaims); ok && parsedToken.Valid {
		if claims["album"] != album {
			return false
		}
		switch expire := claims["expire"].(type) {
		case float64:
			expireTime := time.Unix(int64(expire), 0)
			now := tokenManager.timeProvider.Now()
			if !now.Before(expireTime) {
				log.Printf("Token expired")
				return false
			}
		case json.Number:
			log.Printf("expire has type json.Number")
			return false
		default:
			log.Printf("Unknown type for expire")
			return false
		}
	}
	return true
}
