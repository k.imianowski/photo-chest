package controller

import (
	"log"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

type AuthRequired interface {
	Handler(*gin.Context)
}

type AuthRequiredImpl struct {
}

func (auth *AuthRequiredImpl) Handler(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")
	if user == nil {
		log.Println("User not logged in")
		c.Redirect(http.StatusMovedPermanently, "/login")
		c.Abort()
		return
	}
	c.Next()
}

func NewAuthRequiredImpl() *AuthRequiredImpl {
	return &AuthRequiredImpl{}
}
