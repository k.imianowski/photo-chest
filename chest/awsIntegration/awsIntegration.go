package awsIntegration

import (
	"bytes"
	"fmt"
	"log"
	"path"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"imiano.pl/chest/globals"
	"imiano.pl/chest/model"
)

type AwsIntegration interface {
	GetImg(bucket, imgKey string) (*model.Image, error)
	ListImagesInAlbum(bucket, album string) ([]string, error)
	GetCredentials(username string) (*model.UserCredentials, error)
	ListAlbums() ([]model.Album, error)
	GetAlbum(albumId string) (*model.Album, error)
	SaveAlbum(album *model.Album) error
}

type RealAwsIntegration struct {
	sess *session.Session
}

func NewRealAwsIntegration() *RealAwsIntegration {
	cfg := aws.NewConfig().
		WithRegion(globals.Get().AwsRegion).
		WithLogLevel(aws.LogDebugWithEventStreamBody)
	return &RealAwsIntegration{
		sess: session.Must(session.NewSession(cfg)),
	}
}

func (integration *RealAwsIntegration) GetImg(bucket, imgKey string) (*model.Image, error) {
	log.Printf("Starting pic handler")

	downloader := s3manager.NewDownloader(integration.sess)

	buffer := aws.NewWriteAtBuffer([]byte{})
	input := s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(imgKey),
	}
	n, err := downloader.Download(buffer, &input)
	if err != nil {
		log.Printf("failed to download file, %v", err)
		return nil, fmt.Errorf("Failed to download image")
	}
	return &model.Image{N: n, Reader: bytes.NewReader(buffer.Bytes())}, nil
}

func (integration *RealAwsIntegration) ListImagesInAlbum(bucket, album string) ([]string, error) {
	s3Service := s3.New(integration.sess)

	input := s3.ListObjectsInput{
		Bucket: aws.String(bucket),
		Prefix: aws.String(fmt.Sprintf("resized/%s", album)),
	}

	response, err := s3Service.ListObjects(&input)
	if err != nil {
		return []string{}, fmt.Errorf("Failed to list objects")
	}
	imageList := make([]string, 0)
	for _, item := range response.Contents {
		log.Printf("Got item %v", item)
		fname := path.Base(*item.Key)
		if strings.HasSuffix(fname, ".jpg") || strings.HasSuffix(fname, ".jpeg") {
			imageList = append(imageList, fname)
		}
	}
	return imageList, nil
}

func (integration *RealAwsIntegration) getItemFromDb(dbName, keyName, keyValue string) (*map[string]*dynamodb.AttributeValue, error) {
	svc := dynamodb.New(integration.sess)
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(dbName),
		Key: map[string]*dynamodb.AttributeValue{
			keyName: {
				S: aws.String(keyValue),
			},
		},
	})
	if err != nil {
		log.Printf("Got error calling GetItem: %s", err)
		return nil, fmt.Errorf("Failed to get message from dynamodb")
	}
	if result.Item == nil {
		log.Printf("Could not find message for user")
		return nil, fmt.Errorf("No message found")
	}

	return &result.Item, nil
}

func (integration *RealAwsIntegration) GetCredentials(username string) (*model.UserCredentials, error) {
	item, err := integration.getItemFromDb(globals.Get().UsersTableName, "User", username)
	if err != nil {
		return nil, err
	}

	credentials := model.UserCredentials{}

	err = dynamodbattribute.UnmarshalMap(*item, &credentials)
	if err != nil {
		log.Printf("Failed to unmarshal Record, %v", err)
		return nil, fmt.Errorf("unmarshalling error")
	}
	return &credentials, nil
}

func (integration *RealAwsIntegration) ListAlbums() ([]model.Album, error) {
	svc := dynamodb.New(integration.sess)
	params := &dynamodb.ScanInput{
		TableName: aws.String(globals.Get().AlbumsTableName),
	}

	result, err := svc.Scan(params)
	if err != nil {
		log.Printf("Error retrieving album list %v", err)
		return nil, err
	}

	albums := []model.Album{}
	for _, item := range result.Items {
		album := model.Album{}
		err = dynamodbattribute.UnmarshalMap(item, &album)
		if err != nil {
			log.Printf("Error unmarshalling %v", err)
			return nil, err
		}
		albums = append(albums, album)
	}
	return albums, nil
}

func (integration *RealAwsIntegration) GetAlbum(albumId string) (*model.Album, error) {
	item, err := integration.getItemFromDb(globals.Get().AlbumsTableName, "BucketSubdirName", albumId)
	if err != nil {
		return nil, err
	}

	album := model.Album{}
	err = dynamodbattribute.UnmarshalMap(*item, &album)
	if err != nil {
		log.Printf("Failed to unmarshal Record, %v", err)
		return nil, fmt.Errorf("unmarshalling error")
	}
	return &album, nil
}

func (integration *RealAwsIntegration) SaveAlbum(album *model.Album) error {
	svc := dynamodb.New(integration.sess)
	input := &dynamodb.PutItemInput{
		Item: map[string]*dynamodb.AttributeValue{
			"BucketSubdirName": {
				S: aws.String(album.BucketSubdirName),
			},
			"Name": {
				S: aws.String(album.Name),
			},
			"Password": {
				S: aws.String(album.Password),
			},
			"Description": {
				S: aws.String(album.Description),
			},
		},
		TableName: aws.String(globals.Get().AlbumsTableName),
	}

	_, err := svc.PutItem(input)
	return err
}
