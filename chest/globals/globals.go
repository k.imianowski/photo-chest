package globals

import (
	"encoding/base64"
	"fmt"
	"os"
	"sync"

	"log"
)

type Globals struct {
	ResizedPhotosBucketName string
	UsersTableName          string
	AlbumsTableName         string
	TemplatesPath           string
	PathPrefix              string
	AwsRegion               string
	TokenExpiration         string
	Secret                  []byte
}

var lock = &sync.Mutex{}
var instance *Globals

func Get() *Globals {
	lock.Lock()
	defer lock.Unlock()

	if instance == nil {
		instance = loadGlobals()
	}
	return instance
}

func getFromEnvOrDefault(varName, defaultValue string) string {
	value, found := os.LookupEnv("IMIANO_CHEST_" + varName)
	if !found {
		return defaultValue
	} else {
		return value
	}
}

func getBytesFromEnvOrDefaultWithWarning(varName, defaultValue string) []byte {
	valueBase64, found := os.LookupEnv("IMIANO_CHEST_" + varName)
	if !found {
		log.Printf("WARNING: value of %s taken from defaults. Do not use in production!", varName)
		return []byte(defaultValue)
	} else {
		value, err := base64.RawStdEncoding.WithPadding(base64.StdPadding).DecodeString(valueBase64)
		if err != nil {
			panic(fmt.Sprintf("Unable to decode %s value %s %s", varName, valueBase64, err))
		}
		return value
	}
}

func loadGlobals() *Globals {
	return &Globals{
		ResizedPhotosBucketName: getFromEnvOrDefault("RESIZED_BUCKET_NAME", "imiano-photos-bucket-resized"),
		UsersTableName:          getFromEnvOrDefault("USERS_TABLE_NAME", "usersTable"),
		AlbumsTableName:         getFromEnvOrDefault("ALBUMS_TABLE_NAME", "albumsTable"),
		TemplatesPath:           getFromEnvOrDefault("TEMPLATES_PATH", "templates/*"),
		PathPrefix:              getFromEnvOrDefault("PATH_PREFIX", ""),
		AwsRegion:               getFromEnvOrDefault("AWS_REGION", "eu-central-1"),
		TokenExpiration:         getFromEnvOrDefault("TOKEN_EXPIRATION", "15m"),

		Secret: getBytesFromEnvOrDefaultWithWarning("SECRET", "defaultSecret"),
	}
}
