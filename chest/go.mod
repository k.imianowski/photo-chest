module imiano.pl/chest

go 1.16

require github.com/aws/aws-lambda-go v1.32.0

require github.com/awslabs/aws-lambda-go-api-proxy v0.13.2

require (
	github.com/aws/aws-sdk-go v1.44.45
	github.com/gin-contrib/sessions v0.0.5
	github.com/gin-gonic/gin v1.7.7
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.8.0
	golang.org/x/sys v0.0.0-20220627191245-f75cf1eec38b // indirect
)
